from django.apps import AppConfig


class DjangolabsConfig(AppConfig):
    name = 'DjangoLabs'
