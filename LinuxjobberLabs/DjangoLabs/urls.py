"""LinuxjobberLabs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path
from DjangoLabs import views


urlpatterns = [
    path('', views.index, name = 'index'),
    path('Lab/1', views.Lab_1, name = 'django_lab_1'),
    path('Lab/2', views.Lab_2, name = 'django_lab_2'),
    path('Lab/3', views.Lab_3, name = 'django_lab_3'),
    path('Lab/4', views.Lab_4, name = 'django_lab_4'),
    path('Lab/5', views.Lab_5, name = 'django_lab_5'),
    path('Lab/6', views.Lab_6, name = 'django_lab_6'),
    path('Lab/7', views.Lab_7, name = 'django_lab_7'),
    path('Lab/8', views.Lab_8, name = 'django_lab_8'),
    path('Lab/9', views.Lab_9, name = 'django_lab_9'),
    path('Lab/10', views.Lab_10, name = 'django_lab_10'),
    path('Lab/11', views.Lab_11, name = 'django_lab_11'),
    path('Lab/12', views.Lab_12, name = 'django_lab_12'),
    path('Lab/13', views.Lab_13, name = 'django_lab_13'),
    path('Lab/14', views.Lab_14, name = 'django_lab_14'),
    path('Lab/15', views.Lab_15, name = 'django_lab_15'),
    path('Lab/16', views.Lab_16, name = 'django_lab_16'),
    path('Lab/17', views.Lab_17, name = 'django_lab_17'),
    path('Lab/18', views.Lab_18, name = 'django_lab_18'),
    path('Lab/19', views.Lab_19, name = 'django_lab_19'),
    path('Lab/20', views.Lab_20, name = 'django_lab_20'),
]

