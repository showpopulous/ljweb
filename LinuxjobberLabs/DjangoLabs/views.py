import os


from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

#################################################
#	IMPORTS FROM WITHIN DJANGOLABS APPLICATION  #

from DjangoLabs.models import LabTasks, Lab
from DjangoLabs.forms import DocumentUploadForm
from DjangoLabs.utils.djangolabsutils import handle_lab2_uploaded_file, handle_lab3_uploaded_file, handle_lab4_uploaded_file, handle_lab5_uploaded_file

#################################################

def index(request):

	return render(request, "index.html")


def Lab_1(request):
	lab_queryset = Lab.objects.get(id = 1)
	lab_task_queryset = LabTasks.objects.filter(lab_id = 1)


def Lab_2(request):
	lab_queryset = Lab.objects.get(id = 2)
	lab_task_queryset = LabTasks.objects.filter(lab_id = 2)

	result = '' # score of user returned by the uploaded file handler

	if request.method == 'POST':
		form = DocumentUploadForm(request.POST, request.FILES)
		if form.is_valid():
			output = handle_lab2_uploaded_file(request.FILES['document'], request.POST.get("username"))
			return HttpResponse('Out of 100% Your Lab score is: ' + str(output))
	else:
		form = DocumentUploadForm()
	return render(request, "lab.html", {'lab_details' : lab_queryset, 'lab_tasks' : lab_task_queryset, 'form' : form, 'result' : result})

def Lab_3(request):
	lab_queryset = Lab.objects.get(id = 3)
	lab_task_queryset = LabTasks.objects.filter(lab_id = 3)

	if request.method == 'POST':
		form = DocumentUploadForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			output = handle_lab3_uploaded_file(request.FILES['document'])
			return HttpResponse('Out of 100% Your Lab score is: ' + str(output))
	else:
		form = DocumentUploadForm()
	return render(request, "lab.html", {'lab_details' : lab_queryset, 'lab_tasks' : lab_task_queryset, 'form' : form})

def Lab_4(request):
	lab_queryset = Lab.objects.get(id = 4)
	lab_task_queryset = LabTasks.objects.filter(lab_id = 4)

	if request.method == 'POST':
		form = DocumentUploadForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			output = handle_lab4_uploaded_file(request.FILES['document'])
			return HttpResponse('Out of 100% Your Lab score is: ' + str(output))
	else:
		form = DocumentUploadForm()
	return render(request, "lab.html", {'lab_details' : lab_queryset, 'lab_tasks' : lab_task_queryset, 'form':form})

def Lab_5(request):
	lab_queryset = Lab.objects.get(id = 5)
	lab_task_queryset = LabTasks.objects.filter(lab_id = 5)

	form = DocumentUploadForm(request.POST, request.FILES)

	if request.method == 'POST':
		form = DocumentUploadForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			output = handle_lab5_uploaded_file(request.FILES['document'])
			return HttpResponse('Out of 100% Your Lab score is: ' + str(output))
	else:
		form = DocumentUploadForm()

	return render(request, "lab.html", {'lab_details' : lab_queryset, 'lab_tasks' : lab_task_queryset, 'form' : form})

def Lab_6(request):
	return render(request, "lab.html")

def Lab_7(request):
	return render(request, "lab.html")

def Lab_8(request):
	return render(request, "lab.html")

def Lab_9(request):
	return render(request, "lab.html")

def Lab_10(request):
	return render(request, "lab.html")

def Lab_11(request):
	return render(request, "lab.html")

def Lab_12(request):
	return render(request, "lab.html")

def Lab_13(request):
	return render(request, "lab.html")

def Lab_14(request):
	return render(request, "lab.html")

def Lab_15(request):
	return render(request, "lab.html")

def Lab_16(request):
	return render(request, "lab.html")

def Lab_17(request):
	return render(request, "lab.html")

def Lab_18(request):
	return render(request, "lab.html")

def Lab_19(request):
	return render(request, "lab.html")

def Lab_20(request):
	return render(request, "lab.html")
