from django.apps import AppConfig


class PythonlabsConfig(AppConfig):
    name = 'PythonLabs'
